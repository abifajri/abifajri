from django.shortcuts import render, redirect

name = 'Abi Fajri Abdillah'

def index(request):
    response = {'name' : name}
    return render(request,'index.html',response)

def more(request):
    response = {'name' : name}
    return render(request,'more.html',response)
