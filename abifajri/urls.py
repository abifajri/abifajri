from django.urls import include, path
from .views import index, more

urlpatterns = [
    path('', index, name='index'),
    path('more/', more, name='more'),
]
