dj-database-url==0.5.0
Django==2.2.5
gunicorn==19.8.1
psycopg2-binary==2.7.5
pytz==2019.2
SQLAlchemy==1.2.10
sqlparse==0.3.0
whitenoise==3.3.1
