from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Schedule(models.Model):
    activity = models.CharField(max_length=20)
    place = models.CharField(max_length=20)
    category = models.CharField(max_length=20)
    date_time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.activity