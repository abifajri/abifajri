from django.shortcuts import render, redirect

from .models import Schedule
from .forms import ScheduleForm

# Create your views here.
def myschedule(request):
    schedules = Schedule.objects.all().values()

    response = {
        'form': ScheduleForm,
        'schedules' : schedules
    }

    return render(request, 'myschedule.html', response)


def save_activity(request):
    form = ScheduleForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('myschedule:index')
    
    else:
        return redirect('myschedule:index')    

def delete_activity(request, delete_id):
   Schedule.objects.filter(id=delete_id).delete()
   return redirect('myschedule:index')