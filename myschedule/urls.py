from django.urls import include, path
from .views import myschedule, save_activity, delete_activity

app_name = 'myschedule'

urlpatterns = [
    path('', myschedule, name='index'),
    path('save_activity', save_activity, name='save_activity'),
    path('delete_activity/<int:delete_id>',delete_activity,name='delete_activity'),
]
