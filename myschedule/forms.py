from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = '__all__'
        widgets = {
            'activity': forms.TextInput(attrs={'size': 20}),
            'place': forms.TextInput(attrs={'size': 20}),
            'category': forms.TextInput(attrs={'size': 20}),
        }